module.exports = {
	configureWebpack: {
		module: {
			rules: [
				{
					test: /\.scss$/,
					use: [
						'vue-style-loader',
						'css-loader',
						'sass-loader'
					]
				}
			]
		}
	},
	// https://knasmueller.net/vue-js-on-gitlab-pages
	publicPath: process.env.NODE_ENV === 'production' ? '/ss-2020/webprogrammierung/abgabe/' : '/'
}
