export default { prepareData }

/*
 * Die API gibt die Werte eines Coins in den letzten 7 Tagen (in USD) in einem 1-Dimensionalen Array wieder.
 * Diese Funktion mappt die 1-Dimensionalen Werten in einen verschachtelten Array mit [ZeitStempel, Wert].
 * Dieser wird dann von ApexCharts für die Darstellung der Werte konsumiert.
 * Der Zeitstempel wird Mithilfe der aktuellen Zeit berechnet. Die aktuelle Zeit sowie die Zeit vor 7 Tagen
 * werden als obere und untere Schranke gesetzt. Durch das Entnehmen des Indexes eines Wertes und das Teilen
 * durch die Länge des Gesamten Arrays kann die Prozentuale "Position" des Indexes geholt werden.
 * Durch die Funktion (untere Schranke) + (differenz(obere Schranke, untere Schranke)) * (aktueller Index / Länge)
 * kann der Zeitstempel zu jedem Index gemappt werden.
 */
function prepareData (arrayData, name) {
	var currentTime = Date.now()
	/*
	* Um die Zeit 7 Tage zurück zu rechnen, muss man von der currenTime nachfolgendes abziehen:
	* 7 (Tage) * 24 (Stunden) * 60 (Minuten) * 60 (Sekunden) * 1000 (Millisekunden) = 604800000 oder 7 Tage in Millisekunden.
	* Link zur Info hier: https://stackoverflow.com/questions/19910161/javascript-calculating-date-from-today-date-to-7-days-before
	*/
	var weekAgo = currentTime - (7 * 24 * 60 * 60 * 1000)

	var data = [...arrayData]

	var returnData = []

	for (let i = 0; i < data.length; i++) {
		const percentage = i / data.length
		const timeStampOfData = weekAgo + percentage * (currentTime - weekAgo)
		returnData.push([timeStampOfData, data[i]])
	}

	return { name: name, data: returnData }
}
